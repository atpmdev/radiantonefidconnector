package com.alnt.connector.provisioning.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningAttributes;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.access.provisioning.model.IProvisioningStatus;
import com.alnt.access.provisioning.model.IRoleAuditInfo;
import com.alnt.access.provisioning.model.ISystemInformation;
import com.alnt.access.provisioning.model.ProvisioningAttributes;
import com.alnt.access.provisioning.model.SystemInformation;
import com.alnt.access.provisioning.services.IConnectionInterface;
import com.alnt.access.provisioning.utils.ConnectorUtil;
import com.alnt.connector.constants.RadiantoneFIDConnectorConstants;
import com.alnt.connector.exception.RadiantoneFIDConnectorException;
import com.alnt.connector.provisioning.helper.RadiantoneFIDClientHelper;
import com.alnt.connector.provisioning.helper.RadiantoneFIDURLConnection;
import com.alnt.connector.provisioning.model.ProvisioningResult;
import com.alnt.connector.provisioning.model.RoleAuditInfo;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.model.UserDetails;
import com.alnt.connector.provisioning.model.UserInformation;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.common.constants.IExtractionConstants.USER_ENTITLEMENT_KEY;
import com.alnt.extractionconnector.common.model.IUserInformation;
import com.alnt.extractionconnector.common.service.IExtractionInterface;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.exception.ExtractorConnectionException;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author soori
 *
 */
public class RadiantoneFIDConnectionInterface implements IConnectionInterface, IExtractionInterface {

	private static final String CLASS_NAME = "com.alnt.connector.provisioning.services.RadiantoneFIDConnectionInterface";
	private static Logger logger = Logger.getLogger(CLASS_NAME);
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _basedn = null;
	private String externalUserIdAttribute = null;
	private String _statusAttribute = null;
	private String _validToAttribute = null;
	private boolean _enableProvisioningWarnings = true;
	protected Set<String> excludeLogAttrList = null;
	public String _alertAppDateFormat = null;

	public static final String USER_STATUS_ATTRIBUTE = "";
	public static final String USER_VALIDTO_ATTRIBUTE = "";

	public RadiantoneFIDConnectionInterface(Map<String, String> connectionParams) throws RadiantoneFIDConnectorException {
		try {
			_baseUrl = (String) connectionParams.get(RadiantoneFIDConnectorConstants.BASE_URL);
			_apiUserName = (String) connectionParams.get(RadiantoneFIDConnectorConstants.API_USER_NAME);
			_apiPassword = (String) connectionParams.get(RadiantoneFIDConnectorConstants.API_PASSWORD);
			_alertAppDateFormat = (String) connectionParams.get(RadiantoneFIDConnectorConstants.ALERT_APP_DATE_FORMAT);
			_basedn = (String) connectionParams.get(RadiantoneFIDConnectorConstants.BASE_DN);
			_statusAttribute = (String) connectionParams.get(RadiantoneFIDConnectorConstants.USER_STATUS_ATTRIBUTE);
			_validToAttribute = (String) connectionParams.get(RadiantoneFIDConnectorConstants.USER_VALIDTO_ATTRIBUTE);
			if (connectionParams.containsKey(RadiantoneFIDConnectorConstants.ENABLE_PROVISION_WARNINGS)) {
				_enableProvisioningWarnings = Boolean.parseBoolean(connectionParams.get(RadiantoneFIDConnectorConstants.ENABLE_PROVISION_WARNINGS));
			}
			externalUserIdAttribute = RadiantoneFIDConnectorConstants.RADIANTONEFID_USERID;
			String excludeAttributes = connectionParams.get(RadiantoneFIDConnectorConstants.SENSITIVE_ATTRIBUTES);
			if (excludeAttributes != null && excludeAttributes.isEmpty()) {
				excludeLogAttrList = ConnectorUtil.convertStringToList(excludeAttributes, ",");
			}
			if (excludeLogAttrList == null) {
				excludeLogAttrList = new HashSet<String>();
			}
		} catch (Exception _e) {
			throw new RadiantoneFIDConnectorException("connection parameters should be not be NULL");
		}
	}

	// RECON OPERATIONS :: BEGIN
	/**
	 * 
	 * soori -13-Dec-2019 - 6:16:22 pm
	 * 
	 * @param options
	 * @param fetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.role.services.IRoleExtractionConnectionInterface#getAllRoles(java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 */
	@Override
	public void getAllRoles(Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {
			logger.debug(CLASS_NAME + " Start of getAllRoles(),  Begin");
			String[] roles = null;
			List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
			String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_ROLES);
			RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword);
			if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						roles = new Gson().fromJson(String.valueOf(restapiresponseArray), String[].class);
					}
				}
			}
			for (String role : roles) {
				String roleName = role;
				String roleDesc = role;
				IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc);
				rolesList.add(roleInformation);
			}
			callback.processSearchResult(rolesList);
			logger.debug(CLASS_NAME + " getAllRoles(): End of getAllRoles(), ");
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getRoles() : execution failed ", e);
		}
	}

	/**
	 * 
	 * soori -13-Dec-2019 - 6:17:12 pm
	 * 
	 * @param lastRunDate
	 * @param options
	 * @param fetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.role.services.IRoleExtractionConnectionInterface#getIncrementalRoles(java.util.Date, java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 */
	@Override
	public void getIncrementalRoles(Date lastRunDate, Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {
			logger.debug(CLASS_NAME + " Start of getIncrementalRoles(), Begin");
			String[] roles = null;
			List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
			String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_ROLES);
			RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword);
			if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						roles = new Gson().fromJson(String.valueOf(restapiresponseArray), String[].class);
					}
				}
			}
			for (String role : roles) {
				String roleName = role;
				String roleDesc = role;
				IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc);
				rolesList.add(roleInformation);
			}
			callback.processSearchResult(rolesList);
			logger.debug(CLASS_NAME + " getIncrementalRoles(): End  ");
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getIncrementalRoles() : execution failed ", e);
		}

	}

	/**
	 * 
	 * soori -13-Dec-2019 - 6:42:15 pm
	 * 
	 * @param options
	 * @param fetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.user.services.IUserExtractionConnectionInterface#getAllUsersWithCallback(java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 */
	@Override
	public void getAllUsersWithCallback(Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): Start of getAllUsersWithCallback method");
			long start = System.currentTimeMillis();
			int totalUsers = 0;
			List<IUserInformation> userInfoList = new ArrayList<IUserInformation>();
			List<UserDetails> users = null;

			for (char alphabet = 'A'; alphabet <= 'C'; alphabet++) {
				users = new ArrayList<UserDetails>();
				// ?includeAllData=true&filter=startswith(lastname, b) or startswith(lastname, B)
				String filterQuery = "?includeAllData=true&filter=startswith(lastname, " + Character.toLowerCase(alphabet) + ") or startswith(lastname, " + alphabet + ")";
				logger.debug(CLASS_NAME + " getAllUsersWithCallback(): Start of recon for Users with Lastname starts with" + alphabet);
				String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_USERS).concat(filterQuery);
				logger.debug(CLASS_NAME + " getAllUsersWithCallback(): endPoint " + endPoint);
				RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword);
				if (rs2Connection.getResponseCode() != HttpStatus.SC_OK) {
					logger.error(CLASS_NAME + " getAllUsersWithCallback()  Error While fetching Users " + rs2Connection.getResponseMessage());
				} else if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
					String output = bufferedReader.readLine();
					if (output != null) {
						JSONArray restapiresponseArray = new JSONArray(output);
						if (null != restapiresponseArray && restapiresponseArray.length() > 0) {

							Type listType = new TypeToken<List<UserDetails>>() {
							}.getType();
							users = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
						}
					}
				}
				logger.debug(CLASS_NAME + " getAllUsersWithCallback(): found  " + users.size() + "  Users with Lastname starts with" + alphabet);
				totalUsers = totalUsers + users.size();
				if (null != users && !users.isEmpty()) {
					for (UserDetails cardholder : users) {
						IUserInformation userInfo = processExternalUserInfo(cardholder, options, null);
						userInfoList.add(userInfo);
					}
					callback.processSearchResult(userInfoList);
				}
			}
			long end = System.currentTimeMillis();
			float msec = end - start;
			float sec = msec / 1000F;
			float minutes = sec / 60F;
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): total time taken for  " + totalUsers + "  User Recon   is " + minutes + "  minutes");
		} catch (Exception _ex) {
			logger.error(CLASS_NAME + " getAllUsersWithCallback()  Exception While fetching Users " + _ex.getLocalizedMessage());
		}

	}

	/**
	 * 
	 * soori -13-Dec-2019 - 6:42:28 pm
	 * 
	 * @param lastRunDate
	 * @param options
	 * @param intFetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.user.services.IUserExtractionConnectionInterface#getIncrementalUsersWithCallback(java.util.Date, java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 */
	@Override
	public void getIncrementalUsersWithCallback(Date lastRunDate, Map<String, List<ExtractorAttributes>> options, final int intFetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {

		try {
			logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): Start of getIncrementalUsersWithCallback method");
			long start = System.currentTimeMillis();
			int totalUsers = 0;
			List<IUserInformation> userInfoList = new ArrayList<IUserInformation>();
			List<UserDetails> users = null;
			Set<String> processedUsers = new HashSet<String>();

			// find all cardholders who are modified from last Run , intern it will get all cards modified since last run.
			for (char alphabet = 'A'; alphabet <= 'C'; alphabet++) {
				userInfoList = new ArrayList<IUserInformation>();
				users = new ArrayList<UserDetails>();
				// ?includeAllData=true&filter=lastmodified gt datetime'2019-09-09T09:09:09' and (startswith(lastname, a) or startswith(lastname, A))
				String filterQuery = "?includeAllData=true&filter=lastmodified gt datetime'";
				logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): Start of recon for Users with Lastname starts with" + alphabet);
				String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_USERS).concat(filterQuery);
				logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): endPoint " + endPoint);
				RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword);
				if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
					String output = bufferedReader.readLine();
					if (output != null) {
						JSONArray restapiresponseArray = new JSONArray(output);
						if (null != restapiresponseArray && restapiresponseArray.length() > 0) {

							Type listType = new TypeToken<List<UserDetails>>() {
							}.getType();
							users = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
						}
					}
				}
				logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): found  " + users.size() + "  Users with Lastname starts with" + alphabet);
				totalUsers = totalUsers + users.size();
				if (null != users && !users.isEmpty()) {
					for (UserDetails cardholder : users) {
						IUserInformation userInfo = processExternalUserInfo(cardholder, options, null);
						processedUsers.add(getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, cardholder));
						userInfoList.add(userInfo);
					}
					callback.processSearchResult(userInfoList);
				}
			}
			long end = System.currentTimeMillis();
			float msec = end - start;
			float sec = msec / 1000F;
			float minutes = sec / 60F;
			logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): total time taken for  " + totalUsers + "  User Recon   is " + minutes + "  minutes");
		} catch (Exception _ex) {
			logger.error(CLASS_NAME + " getIncrementalUsersWithCallback()  Exception While fetching Users " + _ex.getLocalizedMessage());
		}

	}

	/**
	 * 
	 * @param userDetails
	 * @param options
	 * @param lastRunAt
	 * @return
	 */
	private IUserInformation processExternalUserInfo(UserDetails userDetails, Map<String, List<ExtractorAttributes>> options, String lastRunAt) {
		IUserInformation userInformation = new UserInformation();
		Map<String, Map<String, List<Map<String, Object>>>> allEntitlements = new HashMap<String, Map<String, List<Map<String, Object>>>>();
		Map<String, Object> userAttr = new HashMap<String, Object>();
		if (null != userDetails) {
			userAttr = convertFormat(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, userDetails), options, userAttr, "User");
			userAttr = convertFormat(RadiantoneFIDConnectorConstants.USER_COMMON_NAME_CN, getAttributeValue(RadiantoneFIDConnectorConstants.USER_COMMON_NAME_CN, userDetails), options, userAttr, "User");
			userAttr = convertFormat(RadiantoneFIDConnectorConstants.USER_FAMILYNAME_SN, getAttributeValue(RadiantoneFIDConnectorConstants.USER_FAMILYNAME_SN, userDetails), options, userAttr, "User");

			List<Map<String, Object>> rolesList = new ArrayList<Map<String, Object>>();

			if (null != userDetails.getUserGroups() && userDetails.getUserGroups().length > 0) {
				for (String userRole : userDetails.getUserGroups()) {
					logger.debug(CLASS_NAME + " processExternalUserInfo(): start processing user role information");
					Map<String, Object> processedDataMap = new HashMap<String, Object>();
					processedDataMap = convertFormat(RadiantoneFIDConnectorConstants.ATTR_ROLE_ID, userRole, options, processedDataMap, "Role");
					rolesList.add(processedDataMap);
					logger.debug(CLASS_NAME + " processExternalUserInfo(): finished user role information processing");
				}
			}

			if (rolesList != null && rolesList.size() > 0) {
				Map<String, List<Map<String, Object>>> entitlement = new HashMap<String, List<Map<String, Object>>>();
				entitlement.put(USER_ENTITLEMENT_KEY.ROLES.toString(), rolesList);
				if (userAttr.containsKey(RadiantoneFIDConnectorConstants.USER_USERNAME_UID) && userAttr.get(RadiantoneFIDConnectorConstants.USER_USERNAME_UID) != null) {
					allEntitlements.put(userAttr.get(RadiantoneFIDConnectorConstants.USER_USERNAME_UID).toString(), entitlement);
				} else {
					logger.debug("Alert attribute userId is not mapped to any of external system's attribute or mapped external attriute is null");
				}
			}

			userInformation.setEntitlements(allEntitlements);
			userInformation.setUserDetails(userAttr);
		}
		return userInformation;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:28:18 pm
	 * 
	 * @param fieldName
	 * @param fieldValue
	 * @param options
	 * @param userAttr
	 * @param type
	 * @return
	 *
	 */
	private Map<String, Object> convertFormat(String fieldName, Object fieldValue, Map<String, List<ExtractorAttributes>> options, Map<String, Object> userAttr, String type) {
		try {
			if (options != null && options.size() > 0 && options.containsKey(fieldName)) {
				List<ExtractorAttributes> extractorAttributesList = options.get(fieldName);
				if (extractorAttributesList != null && extractorAttributesList.size() > 0) {
					for (ExtractorAttributes extractorAttribute : extractorAttributesList) {
						boolean typeMatch = (type.equals("User") && extractorAttribute.isUserAttr()) || (type.equals("Badge") && extractorAttribute.isBadgeAttr()) || (type.equals("Role") && extractorAttribute.isRoleAttr());
						if (extractorAttribute != null && typeMatch) {
							userAttr.put(extractorAttribute.getAttributeName(), fieldValue.toString());
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " convertFormat() : Error setting the value for " + fieldName, e);
		}
		return userAttr;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:34:22 pm
	 * 
	 * @param options
	 * @param roleId
	 * @param roleDesc
	 * @return IRoleInformation
	 *
	 */
	private IRoleInformation getRoleInformation(Map<String, List<ExtractorAttributes>> options, String roleId, String roleDesc) {
		IRoleInformation roleInformation = new RoleInformation();
		logger.trace(CLASS_NAME + " getRoleInformation(): start processing role name " + roleId);
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(roleDesc);
		roleInformation.setDescription(roleDesc);
		roleInformation.setValidFrom(null);
		roleInformation.setValidTo(null);
		logger.trace(CLASS_NAME + " getRoleInformation(): finish processing role name " + roleId);
		return roleInformation;
	}

	@Override
	public List<IUserInformation> getIncrementalUsers(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void getUsers(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void invokeUsers(Map arg0, List arg1) throws ExtractorConnectionException {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getAllRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<com.alnt.extractionconnector.common.model.IRoleInformation> getAllRoles(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<com.alnt.extractionconnector.common.model.IRoleInformation> getIncrementalRoles(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getRolesForUser(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void searchRoles(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {
	}

	@Override
	public boolean supportsProvisioning() {
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getAllUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<IUserInformation> getAllUsers(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}
	// RECON OPERATIONS :: END

	// PROVISIONING OPERATIONS :: BEGIN
	/**
	 * 
	 * soori 2019-12-02 - 5:36:27 pm
	 * 
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#testConnection()
	 *
	 *
	 */
	@Override
	public boolean testConnection() throws Exception {
		logger.info(CLASS_NAME + " testConnection(): Start of testConnection method");
		boolean testConnectionPass = false;
		try {
			String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.TEST_ENDPOINT);
			RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword);
			if (rs2Connection.getResponseCode() == 200) {
				logger.debug("Test connection pass , " + rs2Connection.getResponseMessage());
				testConnectionPass = true;
			}
		} catch (Exception _ex) {
			logger.debug("Test connection failed , " + _ex.getLocalizedMessage());
			return false;
		}
		return testConnectionPass;
	}

	/**
	 * 
	 * soori -06-Feb-2020 - 4:16:15 pm
	 * 
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#getAttributes()
	 *
	 */
	@Override
	public List<IProvisioningAttributes> getAttributes() throws Exception {
		List<IProvisioningAttributes> provAttributes = new ArrayList<IProvisioningAttributes>();
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_POSTALCODE, RadiantoneFIDConnectorConstants.USER_POSTALCODE, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_ST, RadiantoneFIDConnectorConstants.USER_ST, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_STREET, RadiantoneFIDConnectorConstants.USER_STREET, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_DEPARTMENT_NUMBER, RadiantoneFIDConnectorConstants.USER_DEPARTMENT_NUMBER, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_DISPLAYNAME, RadiantoneFIDConnectorConstants.USER_DISPLAYNAME, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_DIVISION_OU, RadiantoneFIDConnectorConstants.USER_DIVISION_OU, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_MAIL, RadiantoneFIDConnectorConstants.USER_MAIL, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_EMPLOYEENUMBER, RadiantoneFIDConnectorConstants.USER_EMPLOYEENUMBER, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_DESCRIPTION, RadiantoneFIDConnectorConstants.USER_DESCRIPTION, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_MANAGER, RadiantoneFIDConnectorConstants.USER_MANAGER, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_FAMILYNAME_SN, RadiantoneFIDConnectorConstants.USER_FAMILYNAME_SN, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_COMMON_NAME_CN, RadiantoneFIDConnectorConstants.USER_COMMON_NAME_CN, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_GIVENNAME, RadiantoneFIDConnectorConstants.USER_GIVENNAME, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_ORGANIZATION_O, RadiantoneFIDConnectorConstants.USER_ORGANIZATION_O, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_USERPASSWORD, RadiantoneFIDConnectorConstants.USER_USERPASSWORD, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_PREFERREDLANGUAGE, RadiantoneFIDConnectorConstants.USER_PREFERREDLANGUAGE, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_TITLE, RadiantoneFIDConnectorConstants.USER_TITLE, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, RadiantoneFIDConnectorConstants.USER_USERNAME_UID, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_EMPLOYEETYPE, RadiantoneFIDConnectorConstants.USER_EMPLOYEETYPE, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_MOBILE, RadiantoneFIDConnectorConstants.USER_MOBILE, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_TELEPHONE_NUMBER, RadiantoneFIDConnectorConstants.USER_TELEPHONE_NUMBER, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_HOME_PHONE, RadiantoneFIDConnectorConstants.USER_HOME_PHONE, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_CREATED_TIMESTAMP, RadiantoneFIDConnectorConstants.USER_CREATED_TIMESTAMP, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_MODIFERS_NAME, RadiantoneFIDConnectorConstants.USER_MODIFERS_NAME, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_CREATORS_NAME, RadiantoneFIDConnectorConstants.USER_CREATORS_NAME, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_MODIFIED_TIMESTAMP, RadiantoneFIDConnectorConstants.USER_MODIFIED_TIMESTAMP, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.USER_BASE_DN, RadiantoneFIDConnectorConstants.USER_BASE_DN, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RadiantoneFIDConnectorConstants.OBJECTCLASS, RadiantoneFIDConnectorConstants.OBJECTCLASS, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		// XXX :: these are mandatory while configuring connector , expected to use new Attributes other than above attributes
		provAttributes.add(new ProvisioningAttributes(_statusAttribute, _statusAttribute, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(_validToAttribute, _validToAttribute, true, RadiantoneFIDConnectorConstants.ATTR_TYPE_STRING));
		return provAttributes;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getAttributes(Map params) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori - 8:23:42 pm
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#isUserProvisioned(java.lang.String)
	 */
	@Override
	public ISystemInformation isUserProvisioned(String userId) throws Exception {
		logger.debug(CLASS_NAME + " isUserProvisioned(): Checking isUserProvisioned  with  UserDetails Id  : " + userId);
		ISystemInformation systemInformation = new SystemInformation(false);
		try {
			UserDetails user = getUserByUserDetailsId(userId, null);
			String uidInSystem = getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, user);
			if (null != uidInSystem && uidInSystem == userId) {
				systemInformation.setProvisioned(true);
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " isUserProvisioned(): Error while call to external system", e);
			systemInformation.setProvisioned(false);
		}
		return systemInformation;
	}

	@SuppressWarnings("rawtypes")
	private UserDetails getUserByUserDetailsId(String userId, String userBaseDN) {

		String baseDn = "/" + RadiantoneFIDConnectorConstants.CN_EQUALS + _basedn;
		String restUrl = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_ADD).concat(baseDn);
		// TODO pass User Base DN if it is available
		String queryString = "?filter=" + RadiantoneFIDConnectorConstants.RADIANTONEFID_USERID + "=" + userId;
		restUrl = restUrl.concat(queryString);
		UserDetails userDetail = null;
		ObjectMapper mapper = new ObjectMapper();
		List resources = new ArrayList();
		RadiantoneFIDURLConnection conn = RadiantoneFIDClientHelper.getConnection(restUrl, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword);
		try {
			if (conn.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					resources = (List) (mapper.readValue(String.valueOf(output), Map.class).get("Resources"));
					if (null != resources && !resources.isEmpty()) {
						// XXX need to get Attributes , each attribute is Array.(we have to return Map<key , [])
						// we have to return object to check isProvisoned , isLocked ,
						// userDetails is wrong reference , need to change
						userDetail = mapper.convertValue(resources.get(0), UserDetails.class);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getUserByUserDetailsId(): Error while call to external system", e);
		}
		return userDetail;
	}

	/**
	 * 
	 * soori -13-Dec-2019 - 6:46:46 pm
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#isUserLocked(java.lang.String)
	 *
	 */
	@Override
	public boolean isUserLocked(String userId) throws Exception {
		UserDetails user = getUserByUserDetailsId(userId, null);
		String userStatusInSystem = getAttributeValue(_statusAttribute, user);
		if (null != userStatusInSystem && userStatusInSystem == RadiantoneFIDConnectorConstants.USER_STATUS_ACTIVE) {
			return false;
		}
		return true;
	}

	/**
	 * @param uid , Should not be null , check before passing
	 * @param cn  , Should not be null , check before passing
	 * @return
	 */
	private String createUserBasedn(String uid, String cn) {
		if (cn.contains(RadiantoneFIDConnectorConstants.CN_EQUALS)) {
			return RadiantoneFIDConnectorConstants.USER_USERNAME_UID + RadiantoneFIDConnectorConstants.EQUALS + uid + RadiantoneFIDConnectorConstants.COMMA + cn;
		} else {
			return RadiantoneFIDConnectorConstants.USER_USERNAME_UID + RadiantoneFIDConnectorConstants.EQUALS + uid + RadiantoneFIDConnectorConstants.COMMA + RadiantoneFIDConnectorConstants.CN_EQUALS + cn;
		}
	}

	/**
	 * 
	 * soori -16-Dec-2019 - 7:18:48 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#create(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public IProvisioningResult create(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " create(): Start of Create Method.");
		IProvisioningResult provResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "User is not provisioned !!!");
		boolean userCreated = true;
		OutputStream outputStream = null;
		Map<String, Object> payloadBody = new HashMap<String, Object>();
		Map<String, Object> payload = new HashMap<String, Object>();
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " create() userId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToCreate = parameters.get(externalUserIdAttribute).toString();
				String preparedUserBaseDN = null;
				if (parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN) != null && !parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString().isEmpty()) {
					preparedUserBaseDN = createUserBasedn(userIdToCreate, parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString());
					parameters.remove(RadiantoneFIDConnectorConstants.USER_BASE_DN);
				} else if (null != _basedn) {
					preparedUserBaseDN = createUserBasedn(userIdToCreate, _basedn);
				}
				if (parameters.get(RadiantoneFIDConnectorConstants.OBJECTCLASS) == null) {
					parameters.put(RadiantoneFIDConnectorConstants.OBJECTCLASS, "[]");
					;
				} else {
					String objectClassValue = parameters.get(RadiantoneFIDConnectorConstants.OBJECTCLASS).toString();
					parameters.remove(RadiantoneFIDConnectorConstants.OBJECTCLASS);
					parameters.put(RadiantoneFIDConnectorConstants.OBJECTCLASS, Arrays.asList(objectClassValue.split(";")));
				}
				// XXX : if statusAttribute is not passed adding as Active, to make LOCK,UNLOCK work properly
				// if not we don't know _statusAttribute is a ADD / MODIFY
				if (parameters.get(_statusAttribute) == null) {
					parameters.put(_statusAttribute, RadiantoneFIDConnectorConstants.USER_STATUS_ACTIVE);
				}
				payloadBody.put(RadiantoneFIDConnectorConstants.DN, preparedUserBaseDN);
				payloadBody.put(RadiantoneFIDConnectorConstants.ATTRIBUTES, parameters);
				payload.put(RadiantoneFIDConnectorConstants.PARAMS, payloadBody);
				ObjectMapper mapper = new ObjectMapper();
				String gsonString = mapper.writeValueAsString(payload);
				System.out.println(gsonString);
				String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_ADD);
				RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword);
				logger.debug(CLASS_NAME + "   create()   request payload  :::  " + gsonString);
				outputStream = rs2Connection.getOutputStream();
				outputStream.write(gsonString.getBytes());
				outputStream.flush();
				if (rs2Connection.getResponseCode() != HttpStatus.SC_OK) {
					logger.error(CLASS_NAME + "   create()   " + rs2Connection.getResponseMessage());
					provResult.setMsgDesc(rs2Connection.getResponseMessage());
					return provResult;
				} else if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
					provResult.setUserId(parameters.get(externalUserIdAttribute).toString());
					provResult.setUserCreated(userCreated);
					provResult.setMsgCode(IProvisoiningConstants.CREATE_USER_SUCCESS);
					provResult.setMsgDesc("User provisioned successfully");
					provResult.setProvFailed(false);
					parameters.put(externalUserIdAttribute, parameters.get(externalUserIdAttribute).toString());
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + "create()", e);
			provResult.setMsgDesc(e.getMessage());
			return provResult;
		}
		logger.debug(CLASS_NAME + " create(): End of Create Method.");
		return provResult;
	}

	/**
	 * 
	 * soori -10-Dec-2019 - 5:17:17 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#update(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult update(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " update(): Start of update method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "update user failed in target system!");
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " update() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToUpdate = parameters.get(externalUserIdAttribute).toString();
				UserDetails cardHolder = getUserByUserDetailsId(userIdToUpdate, null);

				if (null != cardHolder) {
					UserDetails cardHolderInRequest = prepareUserDetails(parameters);
					// XXX :: BeanUtils.copyProperties(cardHolder, cardHolderInRequest); //clone using reflection
					// cardHolder.setFirstName(cardHolderInRequest.getFirstName());
					// cardHolder.setLastName(cardHolderInRequest.getLastName());

					String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_USERS).concat("/").concat(userIdToUpdate);
					RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword);
					Gson gson = new Gson();
					String gsonString = gson.toJson(cardHolder);
					logger.debug(CLASS_NAME + "   update()   request payload  :::  " + gsonString);
					outputStream = rs2Connection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					}
				} else {
					if (!_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " update() user does not exist / already updated in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " update() user does not exist / already updated , but warnings are disabled , returning as updated successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " update()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "Error during update the user");
		}
		logger.info(CLASS_NAME + " update(): End of update method");
		return provisioningResult;

	}

	/**
	 * 
	 * 
	 * This method is to delete the user account in external system.
	 * 
	 * soori - 2:16:34 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#deleteAccount(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult deleteAccount(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " deleteAccount(): Start of update method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "Error while deleting User in external system");
		ISystemInformation systemInformation = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " deleteAccount() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIDtoDelete = parameters.get(externalUserIdAttribute).toString();
				systemInformation = isUserProvisioned(userIDtoDelete);
				if (systemInformation != null && systemInformation.isProvisioned()) {
					String preparedUserBaseDN = null;
					if (parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN) != null && !parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString().isEmpty()) {
						preparedUserBaseDN = createUserBasedn(userIDtoDelete, parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString());
						parameters.remove(RadiantoneFIDConnectorConstants.USER_BASE_DN);
					} else if (null != _basedn) {
						preparedUserBaseDN = createUserBasedn(userIDtoDelete, _basedn);
					}
					String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_ADD).concat("/").concat(preparedUserBaseDN);
					RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_DELETE, _apiUserName, _apiPassword);
					if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
						logger.debug(CLASS_NAME + " deleteAccount() user deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
					}
				} else {
					if (!_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " deleteAccount() user does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " deleteAccount() user does not exist , but warnings are disabled , returning as deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " deleteAccount(): Error during update the user ", e);
			logger.error(CLASS_NAME + " deleteAccount()", e);
		}
		return provisioningResult;
	}

	public String getAttributeValue(String AttributeName, UserDetails user) {
		if (null != user && null != user.getAttributes()) {
			Map<String, List<String>> userAttributes = user.getAttributes();
			if (userAttributes.get(_statusAttribute) != null) {
				List<String> userStatusList = userAttributes.get(_statusAttribute);
				if (null != userStatusList && !userStatusList.isEmpty()) {
					return userStatusList.get(0);
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:54:34 am
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#lock(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult lock(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {

		logger.info(CLASS_NAME + " lock(): Start of lock method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "lock user failed in target system!");
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " lock() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToLock = parameters.get(externalUserIdAttribute).toString();
				UserDetails cardHolder = getUserByUserDetailsId(userIdToLock, null);
				if (null != getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, cardHolder)) {
					String userStatusInSystem = getAttributeValue(_statusAttribute, cardHolder);
					if (null != userStatusInSystem && userStatusInSystem.equals(RadiantoneFIDConnectorConstants.USER_STATUS_ACTIVE)) {
						// TODD :: refactor code :: fold same map in map
						Map<String, Object> payload = new HashMap<String, Object>();
						Map<String, Object> modParams = new HashMap<String, Object>();
						Map<String, Object> payloadBody = new HashMap<String, Object>();
						modParams.put(RadiantoneFIDConnectorConstants.ATTRIBUTES, _statusAttribute);
						modParams.put(RadiantoneFIDConnectorConstants.API_ATTRIBUTE_ACTION_TYPE_PROPERTY, RadiantoneFIDConnectorConstants.API_ATTRIBUTE_ACTION_TYPE_REPLACE);
						String[] values = { RadiantoneFIDConnectorConstants.USER_STATUS_INACTIVE };
						modParams.put(RadiantoneFIDConnectorConstants.PAYLOAD_VALUES, values);
						payloadBody.put(RadiantoneFIDConnectorConstants.PAYLOAD_MODS, modParams);
						payload.put(RadiantoneFIDConnectorConstants.PARAMS, payloadBody);
						ObjectMapper mapper = new ObjectMapper();
						String gsonString = mapper.writeValueAsString(payload);
						System.out.println(gsonString);
						String preparedUserBaseDN = null;
						if (parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN) != null && !parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString().isEmpty()) {
							preparedUserBaseDN = createUserBasedn(userIdToLock, parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString());
							parameters.remove(RadiantoneFIDConnectorConstants.USER_BASE_DN);
						} else if (null != _basedn) {
							preparedUserBaseDN = createUserBasedn(userIdToLock, _basedn);
						}
						String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_ADD).concat("/").concat(preparedUserBaseDN);
						RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_PATCH, _apiUserName, _apiPassword);
						logger.debug(CLASS_NAME + "   lock()   request payload  :::  " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, false, null);
						}
					} else {
						if (!_enableProvisioningWarnings) {
							logger.debug(CLASS_NAME + "User already locked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "User already locked in the system");
						} else {
							logger.debug(CLASS_NAME + "User already locked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, true, "User already locked in the system");
						}
					}
				} else {
					logger.debug(CLASS_NAME + "lock() user does not exist");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, false, null);
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " lock()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "Error during lock the user");
		}
		logger.info(CLASS_NAME + " lock(): End of lock method");
		return provisioningResult;

	}

	/**
	 * 
	 * soori - 4:43:19 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#unlock(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult unlock(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " unlock(): Start of unlock method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "unlock user failed in target system!");
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " unlock() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToLock = parameters.get(externalUserIdAttribute).toString();
				UserDetails cardHolder = getUserByUserDetailsId(userIdToLock, null);
				if (null != getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, cardHolder)) {
					String userStatusInSystem = getAttributeValue(_statusAttribute, cardHolder);
					if (null != userStatusInSystem && userStatusInSystem.equals(RadiantoneFIDConnectorConstants.USER_STATUS_INACTIVE)) {
						// TODD :: refactor code :: fold same map in map
						Map<String, Object> payload = new HashMap<String, Object>();
						Map<String, Object> modParams = new HashMap<String, Object>();
						Map<String, Object> payloadBody = new HashMap<String, Object>();
						modParams.put(RadiantoneFIDConnectorConstants.ATTRIBUTES, _statusAttribute);
						modParams.put(RadiantoneFIDConnectorConstants.API_ATTRIBUTE_ACTION_TYPE_PROPERTY, RadiantoneFIDConnectorConstants.API_ATTRIBUTE_ACTION_TYPE_REPLACE);
						String[] values = { RadiantoneFIDConnectorConstants.USER_STATUS_ACTIVE };
						modParams.put(RadiantoneFIDConnectorConstants.PAYLOAD_VALUES, values);
						payloadBody.put(RadiantoneFIDConnectorConstants.PAYLOAD_MODS, modParams);
						payload.put(RadiantoneFIDConnectorConstants.PARAMS, payloadBody);
						ObjectMapper mapper = new ObjectMapper();
						String gsonString = mapper.writeValueAsString(payload);
						String preparedUserBaseDN = null;
						if (parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN) != null && !parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString().isEmpty()) {
							preparedUserBaseDN = createUserBasedn(userIdToLock, parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString());
							parameters.remove(RadiantoneFIDConnectorConstants.USER_BASE_DN);
						} else if (null != _basedn) {
							preparedUserBaseDN = createUserBasedn(userIdToLock, _basedn);
						}
						String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_ADD).concat("/").concat(preparedUserBaseDN);
						RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_PATCH, _apiUserName, _apiPassword);
						logger.debug(CLASS_NAME + "   unlock()   request payload  :::  " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, false, null);
						}
					} else {
						if (!_enableProvisioningWarnings) {
							logger.debug(CLASS_NAME + " unlock() user already unlocked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "User already active in the system");
						} else {
							logger.debug(CLASS_NAME + " unlock() user already unlocked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, true, "User already active in the system");
						}
					}
				} else {
					logger.debug(CLASS_NAME + " unlock() user does not exist");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, false, "User does not exist in external system");
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " unlock()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "Error during unlock the user");
		}
		logger.info(CLASS_NAME + " unlock(): End of lock method");
		return provisioningResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult delimitUser(Long requestNumber, List roles, Map parameters, List requestDetails, String action, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " delimitUser(): Start of delimitUser method");
		OutputStream outputStream = null;

		String failureCode = null, successCode = null;
		successCode = action.equals(IProvisoiningConstants.PROV_ACTION_DELIMIT_USER) ? IProvisoiningConstants.DELIMIT_USER_SUCCESS : IProvisoiningConstants.CHANGE_VALIDITY_DATES_SUCCESS;
		failureCode = action.equals(IProvisoiningConstants.PROV_ACTION_DELIMIT_USER) ? IProvisoiningConstants.DELIMIT_USER_FAILURE : IProvisoiningConstants.CHANGE_VALIDITY_DATES_FAILURE;

		IProvisioningResult provisioningResult = prepareProvisioningResult(action, failureCode, true, " unsuccessfull !!");
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " delimitUser() userId does not passed!!");
				return prepareProvisioningResult(action, failureCode, true, "userId does not passed!!");
			}
			// we will not get ValidAttribute , we will get _validToAttribute
			if (parameters.get(_validToAttribute) == null) {
				logger.debug(CLASS_NAME + " delimitUser() userexpirydate is not passed!!");
				return prepareProvisioningResult(action, failureCode, true, "user expirydate does not passed!!");
			}
			// ideally change validity dates will change both dates  valid From besides Valid From, for radaint is it not applicable
			// if required , need another connector parameter for valid From and update
			/*
			if (action.equals(IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES) && parameters.get(RadiantoneFIDConnectorConstants.USER_VALIDTO_ATTRIBUTE) == null) {
				logger.debug(CLASS_NAME + " delimitUser() userstartdate is not passed for change validity dates action!!");
				return prepareProvisioningResult(action, failureCode, true, "user startdate does not passed for change validity dates action!!");
			} */
			String userIdToUpdate = parameters.get(externalUserIdAttribute).toString();
			UserDetails userDetails = getUserByUserDetailsId(userIdToUpdate, _basedn);
			if (null != userDetails) {
				Map<String, Object> payload = new HashMap<String, Object>();
				Map<String, Object> modParams = new HashMap<String, Object>();
				Map<String, Object> payloadBody = new HashMap<String, Object>();
				modParams.put(RadiantoneFIDConnectorConstants.ATTRIBUTES, _validToAttribute);
				modParams.put(RadiantoneFIDConnectorConstants.API_ATTRIBUTE_ACTION_TYPE_PROPERTY, RadiantoneFIDConnectorConstants.API_ATTRIBUTE_ACTION_TYPE_REPLACE);
				String[] values = { parseDateStringToDateString((String)parameters.get(_validToAttribute), _alertAppDateFormat, RadiantoneFIDConnectorConstants.DEFAULT_DATE_FORMAT) };
				modParams.put(RadiantoneFIDConnectorConstants.PAYLOAD_VALUES, values);
				payloadBody.put(RadiantoneFIDConnectorConstants.PAYLOAD_MODS, modParams);
				payload.put(RadiantoneFIDConnectorConstants.PARAMS, payloadBody);
				ObjectMapper mapper = new ObjectMapper();
				String gsonString = mapper.writeValueAsString(payload);
				System.out.println(gsonString);
				String preparedUserBaseDN = null;
				if (parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN) != null && !parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString().isEmpty()) {
					preparedUserBaseDN = createUserBasedn(userIdToUpdate, parameters.get(RadiantoneFIDConnectorConstants.USER_BASE_DN).toString());
					parameters.remove(RadiantoneFIDConnectorConstants.USER_BASE_DN);
				} else if (null != _basedn) {
					preparedUserBaseDN = createUserBasedn(userIdToUpdate, _basedn);
				}
				String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_ADD).concat("/").concat(preparedUserBaseDN);
				RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_PATCH, _apiUserName, _apiPassword);
				logger.debug(CLASS_NAME + "   delimitUser()   request payload  :::  " + gsonString);
				outputStream = rs2Connection.getOutputStream();
				outputStream.write(gsonString.getBytes());
				outputStream.flush();
				if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
					provisioningResult = prepareProvisioningResult(action, successCode, false, null);
				}
			} else {
				if (_enableProvisioningWarnings) {
					logger.debug(CLASS_NAME + " delimitUser() user does not exist ");
					provisioningResult = prepareProvisioningResult(action, failureCode, true, "User does not exist in external system");
				} else {
					logger.debug(CLASS_NAME + " delimitUser() user does not exist , but warnings are disabled , returning as delimited successfully in the system");
					provisioningResult = prepareProvisioningResult(action, successCode, false, null);
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " delimitUser(): Error during delimitUser the user ", e);
			logger.error(CLASS_NAME + " delimitUser()", e);
			provisioningResult = prepareProvisioningResult(action, failureCode, true, "Error during delimitUser user");
		}
		logger.info(CLASS_NAME + " delimitUser(): End of delimitUser method");
		return provisioningResult;
	}

	private String parseDateStringToDateString(String dateString, String currentFormat, String acceptedFormat) throws Exception {
		try {
			logger.debug(CLASS_NAME + " parseDateString() dateString to parse :: " + dateString);
			logger.debug(CLASS_NAME + " parseDateString() dateString  current format :: " + dateString);
			logger.debug(CLASS_NAME + " parseDateString() dateString  acceptedFormat :: " + acceptedFormat);
			DateTimeFormatter oldPattern = DateTimeFormatter.ofPattern(currentFormat);
			DateTimeFormatter newPattern = DateTimeFormatter.ofPattern(acceptedFormat);
			LocalDateTime datetime = LocalDateTime.parse(dateString, oldPattern);
			return datetime.format(newPattern);
		} catch (Exception e) {
			throw e;
		}
	}
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult activateBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult deActivateBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult addBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult addTempBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-02 - 5:38:47 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param deprovisionRole
	 * @param attMappin
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#changeAccess(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public IProvisioningResult changeAccess(Long requestNumber, List provisionRoles, Map parameters, List requestDetails, List<IRoleInformation> deprovisionRoles, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " changeAccess(): Start of changeAccess method");
		IProvisioningResult provisioningResult = null;
		OutputStream outputStream = null;
		List<IRoleAuditInfo> roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
		try {
			if ((deprovisionRoles != null && !deprovisionRoles.isEmpty()) || (provisionRoles != null && !provisionRoles.isEmpty())) {
				if (parameters.get(externalUserIdAttribute) == null) {
					logger.debug(CLASS_NAME + " changeAccess() userId does not passed!!");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "UserId does not passed!!");
				} else {
					String userIdToChangeAccess = parameters.get(externalUserIdAttribute).toString();
					UserDetails cardHolder = getUserByUserDetailsId(userIdToChangeAccess, null);
					if (null != cardHolder) {
						String[] rolesInSystem = cardHolder.getUserGroups();
						if (deprovisionRoles != null && !deprovisionRoles.isEmpty()) {
							Iterator<IRoleInformation> removeRoleItr = deprovisionRoles.iterator();
							while (removeRoleItr.hasNext()) {
								IRoleInformation roleInformation = (IRoleInformation) removeRoleItr.next();
								for (int i = 0; i < rolesInSystem.length; i++) {
									if (rolesInSystem[i].equals(roleInformation.getName())) {
										for (int j = i; j < rolesInSystem.length - 1; j++) {
											rolesInSystem[j] = rolesInSystem[j + 1];
										}
										break;
									}
								}
								roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.REMOVE_ROLE));
							}
						}
						if (provisionRoles != null && !provisionRoles.isEmpty()) {
							Iterator<IRoleInformation> addRoleItr = provisionRoles.iterator();
							while (addRoleItr.hasNext()) {
								ArrayList<String> rolesInSystemList = new ArrayList<String>(Arrays.asList(rolesInSystem));
								IRoleInformation roleInformation = (IRoleInformation) addRoleItr.next();
								rolesInSystemList.add(roleInformation.getName());
								roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.ADD_ROLE));
								rolesInSystem = rolesInSystemList.toArray(rolesInSystem);
							}
						}
						cardHolder.setUserGroups(rolesInSystem);
						String endPoint = _baseUrl.concat(RadiantoneFIDConnectorConstants.ENDPOINT_USERS).concat("/").concat(userIdToChangeAccess);
						RadiantoneFIDURLConnection rs2Connection = RadiantoneFIDClientHelper.getConnection(endPoint, RadiantoneFIDConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword);
						Gson gson = new Gson();
						String gsonString = gson.toJson(cardHolder);
						logger.debug(CLASS_NAME + "   changeAccess()   request payload  :::  " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, "Access changed successfully.");
						} else {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error assigning roles");
						}
					} else {
						if (!_enableProvisioningWarnings) {
							logger.debug(CLASS_NAME + " changeAccess() user does not exist ");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "User does not exist in external system");
						} else {
							logger.debug(CLASS_NAME + " changeAccess() user does not exist , but warnings are disabled , returning as change Access successfully in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, null);
						}
					}
				}
			} else {
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, "Access changed successfully.");
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " changeAccess(): Error Assigning the roles = ", e);
			roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error assigning roles");
		}
		provisioningResult.setRoleList(roleAuditInfoList);
		return provisioningResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult changeBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult changeBadgeRoles(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult removeBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-13 - 12:01:43 pm
	 * 
	 * @param roleInformation
	 * @param action
	 * @return IRoleAuditInfo
	 *
	 */
	private IRoleAuditInfo getRoleAuditInfo(IRoleInformation roleInformation, String action) {
		IRoleAuditInfo roleAuditInfo = new RoleAuditInfo();
		roleAuditInfo.setRoleName(roleInformation.getName());
		roleAuditInfo.setValidFrom(roleInformation.getValidFrom());
		roleAuditInfo.setValidTo(roleInformation.getValidTo());
		roleAuditInfo.setAction(action);
		return roleAuditInfo;
	}

	/**
	 * 
	 * @param provisioningAction
	 * @param msgCode
	 * @param isFailed
	 * @param msgDesc
	 * @return IProvisioningResult
	 */

	private IProvisioningResult prepareProvisioningResult(String provisioningAction, String msgCode, Boolean isFailed, String msgDesc) {
		IProvisioningResult provisioningResult = new ProvisioningResult();
		provisioningResult.setMsgCode(msgCode);
		if (null != isFailed) {
			provisioningResult.setProvFailed(isFailed);
		}
		provisioningResult.setMsgDesc(msgDesc);
		provisioningResult.setProvAction(provisioningAction);
		return provisioningResult;
	}

	/**
	 * 
	 * @param userAttributesMap
	 * @return UserDetails
	 */
	@SuppressWarnings({ "rawtypes" })
	public UserDetails prepareUserDetails(Map userAttributesMap) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		UserDetails userDetails = mapper.convertValue(userAttributesMap, UserDetails.class);
		return userDetails;
	}

	@Override
	public void setTaskId(Long arg0) {
	}

	@Override
	public IProvisioningResult updatePassword(Long arg0, String arg1, String arg2, String arg3) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getBadgeLastLocation(String arg0) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getDetailsAsList(String arg0, String arg1) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getExistingBadges(String arg0) throws Exception {

		return null;
	}

	@Override
	public IProvisioningStatus getProvisioningStatus(Map<String, String> arg0) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List provision(Long arg0, String arg1, List arg2, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg3, Map arg4, List arg5, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg6,
			Map<String, String> arg7) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	public IProvisioningResult create(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4, Map<String, String> arg5) throws Exception {
		return null;
	}
	// PROVISIONING OPERATIONS :: BEGIN

}
