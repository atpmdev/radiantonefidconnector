package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDetails implements Serializable {

	private static final long serialVersionUID = -6507575691173675174L;

	private String[] UserGroups;
	private Map<String, List<String>> attributes = new HashMap<String, List<String>>();

	public String[] getUserGroups() {
		return UserGroups;
	}

	public void setUserGroups(String[] userGroups) {
		UserGroups = userGroups;
	}

	public Map<String, List<String>> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, List<String>> attributes) {
		this.attributes = attributes;
	}

}
