package com.alnt.connector.provisioning.helper;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import com.alnt.connector.constants.RadiantoneFIDConnectorConstants;

public class RadiantoneFIDClientHelper {

	final private static String CLASS_NAME = RadiantoneFIDClientHelper.class.getName();
	static Logger logger = Logger.getLogger(CLASS_NAME);

	public static final boolean isHttpsProtocal(String restUrl) {
		return !restUrl.isEmpty() && restUrl.startsWith("https://");
	}

	public static RadiantoneFIDURLConnection getConnection(final String restURL, final String methodType, final String userId, final String password) {
		if (isHttpsProtocal(restURL)) {
			return getRESTHttpsServiceConnection(restURL.trim(), methodType, userId, password);
		} else {
			return getRESTHttpServiceConnection(restURL.trim(), methodType, userId, password);
		}
	}

	public static RadiantoneFIDURLConnection getRESTHttpServiceConnection(final String restURL, final String methodType, final String userId, final String password) {
		final String METHOD_NAME = "getRESTServiceConnection()";
		logger.debug(CLASS_NAME + "getRESTHttpServiceConnection  :: " + "restUrl" + restURL + "method type: " + methodType);
		RadiantoneFIDHttpURLConnection restHttpURLConnection = new RadiantoneFIDHttpURLConnection();
		HttpURLConnection conn = null;
		URL url = null;
		try {
			logger.debug(CLASS_NAME + METHOD_NAME + "Building URL:" + restURL + " methodType:" + methodType);
			String escapeRestURL = restURL.replaceAll(" ", "%20");
			url = new URL(escapeRestURL);
			logger.debug(CLASS_NAME + METHOD_NAME + "Opening connection");
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(methodType);
			conn.setRequestProperty(RadiantoneFIDConnectorConstants.ACCEPT_TYPE, RadiantoneFIDConnectorConstants.JSON_CONTENT);
			conn.setRequestProperty(RadiantoneFIDConnectorConstants.CONTENT_TYPE, RadiantoneFIDConnectorConstants.JSON_CONTENT);
			String userpass = userId + RadiantoneFIDConnectorConstants.USERID_PWD_SEPERATOR + password;
			String basicAuth = RadiantoneFIDConnectorConstants.AUTHORIZATION_TYPE + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
			conn.setRequestProperty(RadiantoneFIDConnectorConstants.REQ_HEADER_AUTHORIZATION, basicAuth);
			conn.setDoOutput(true);
			restHttpURLConnection.setHttpURLConnection(conn);
			logger.debug(CLASS_NAME + METHOD_NAME + "returning HttpURLConnection connection");
		} catch (Exception ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		logger.debug(CLASS_NAME + METHOD_NAME + "ENDS");
		return restHttpURLConnection;
	}

	public static RadiantoneFIDURLConnection getRESTHttpsServiceConnection(final String restURL, final String methodType, final String userId, final String password) {
		final String METHOD_NAME = "getRESTServiceConnection()";
		logger.debug(CLASS_NAME + METHOD_NAME + "Inside : getRESTServiceConnection ,restUrl" + restURL + "method type: " + methodType);
		RadiantoneFIDHttpsURLConnection restHttpsURLConnection = new RadiantoneFIDHttpsURLConnection();
		HttpsURLConnection conn = null;
		URL url = null;
		try {
			logger.debug(CLASS_NAME + METHOD_NAME + "Building URL:" + restURL + " methodType:" + methodType);
			String escapeRestURL = restURL.replaceAll(" ", "%20");
			url = new URL(escapeRestURL);
			logger.debug(CLASS_NAME + METHOD_NAME + "Opening connection");
			conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod(methodType);
			conn.setRequestProperty(RadiantoneFIDConnectorConstants.ACCEPT_TYPE, RadiantoneFIDConnectorConstants.JSON_CONTENT);
			conn.setRequestProperty(RadiantoneFIDConnectorConstants.CONTENT_TYPE, RadiantoneFIDConnectorConstants.JSON_CONTENT);
			String userpass = userId + RadiantoneFIDConnectorConstants.USERID_PWD_SEPERATOR + password;
			String basicAuth = RadiantoneFIDConnectorConstants.AUTHORIZATION_TYPE + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
			conn.setRequestProperty(RadiantoneFIDConnectorConstants.REQ_HEADER_AUTHORIZATION, basicAuth);
			conn.setDoOutput(true);
			restHttpsURLConnection.setHttpsURLConnection(conn);
			logger.debug(CLASS_NAME + METHOD_NAME + "returning HttpURLConnection connection");
		} catch (Exception ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		logger.debug(CLASS_NAME + METHOD_NAME + "ENDS");
		return restHttpsURLConnection;
	}
}
