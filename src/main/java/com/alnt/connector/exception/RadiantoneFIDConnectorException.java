package com.alnt.connector.exception;

public class RadiantoneFIDConnectorException extends Exception{

	private static final long serialVersionUID = 410782776743392490L;

	public RadiantoneFIDConnectorException(String message) {
		super(message);
	}
}
