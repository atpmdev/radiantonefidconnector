package com.alnt.connector.constants;

public class RadiantoneFIDConnectorConstants {

	public static final String CONNECTOR_NAME = "RadiantoneFIDConnector";
	public static final String RADIANTONEFID_USERID = "uid";
	public static final String AE_USERID = "UserId";
	public static final String DEFAULT_DATE_FORMAT = "yyyyMMddHHmmss.SSSZ";

	// CONNECTOR CONNECTION SPECIFIC
	public static final String API_USER_NAME = "userName";
	public static final String API_PASSWORD = "password";
	public static final String BASE_URL = "baseURL";
	public static final String ENABLE_PROVISION_WARNINGS = "enableProvisioningWarnings";
	public static final String ALERT_APP_DATE_FORMAT = "alertAppDateFormat";
	public static final String BASE_DN = "baseDn";
	public static final String SENSITIVE_ATTRIBUTES = "sensitiveAttributes";
	public static final String USER_STATUS_ATTRIBUTE = "statusAttribute";
	public static final String USER_VALIDTO_ATTRIBUTE = "validToAttribute";
	

	public static final String REQ_HEADER_AUTHORIZATION = "Authorization";
	public static final String AUTHORIZATION_TYPE = "Basic ";
	public static final String USERID_PWD_SEPERATOR = ":";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String ACCEPT_TYPE = "Accept";
	public static final String JSON_CONTENT = "application/json";
	

	// GENERIC METADATA
	public static final String REQ_METHOD_TYPE_GET = "GET";
	public static final String REQ_METHOD_TYPE_POST = "POST";
	public static final String REQ_METHOD_TYPE_HEAD = "HEAD";
	public static final String REQ_METHOD_TYPE_OPTIONS = "OPTIONS";
	public static final String REQ_METHOD_TYPE_PUT = "PUT";
	public static final String REQ_METHOD_TYPE_DELETE = "DELETE";
	public static final String REQ_METHOD_TYPE_TRACE = "TRACE";
	public static final String REQ_METHOD_TYPE_PATCH = "PATCH";

	public static final String ATTR_TYPE_STRING = "String";
	public static final String ATTR_TYPE_INT = "Int";
	public static final String ATTR_TYPE_BOOLEAN = "Boolean";
	public static final String ATTR_TYPE_DATE = "Date";
	public static final String  EQUALS = "=";
	public static final String  COMMA = ",";

	// API ENDPOINTS
	public static final String ENDPOINT_USERS = "api/externalAccount/user";
	public static final String ENDPOINT_ROLES = "api/externalAccount/groups";
	public static final String TEST_ENDPOINT="adap?bind=simpleBind";
	public static final String ENDPOINT_CREATEUSER="adap";
	public static final String ENDPOINT_ADD="adap";
	


	public static final String ATTR_ROLE_ID ="RoleID";
	
	
	public static final String  USER_POSTALCODE = "postalCode";
	public static final String  USER_ST = "st";
	public static final String  USER_STREET = "street";
	public static final String  USER_DEPARTMENT_NUMBER = "departmentNumber";
	public static final String  USER_DISPLAYNAME = "displayName";
	public static final String  USER_DIVISION_OU = "ou";
	public static final String  USER_MAIL = "mail";
	public static final String  USER_EMPLOYEENUMBER = "employeeNumber";
	public static final String  USER_DESCRIPTION = "description";
	public static final String  USER_MANAGER = "manager";
	public static final String  USER_FAMILYNAME_SN = "sn";
	public static final String  USER_COMMON_NAME_CN = "cn";
	public static final String  USER_GIVENNAME = "givenName";
	public static final String  USER_ORGANIZATION_O = "o";
	public static final String  USER_USERPASSWORD = "userPassword";
	public static final String  USER_PREFERREDLANGUAGE = "preferredLanguage";
	public static final String  USER_TITLE = "title";
	public static final String  USER_USERNAME_UID = "uid";
	public static final String  USER_EMPLOYEETYPE = "employeeType";
	public static final String  USER_MOBILE = "mobile";
	public static final String  USER_TELEPHONE_NUMBER = "telephoneNumber";
	public static final String  USER_HOME_PHONE = "homePhone";
	public static final String  USER_CREATED_TIMESTAMP = "createdTimeStamp";
	public static final String  USER_MODIFERS_NAME = "modifersName";
	public static final String  USER_CREATORS_NAME = "creatorsName";
	public static final String  USER_MODIFIED_TIMESTAMP = "modifiedTimeStamp";
	public static final String  USER_BASE_DN = "userBaseDn";
	public static final String  OBJECTCLASS = "objectClass";
	// pay load specific
	public static final String  DN = "dn";
	public static final String  ATTRIBUTES = "attributes";
	public static final String  PARAMS = "params";
	
	
	public static final String  CN_EQUALS = "cn=";
	public static final String USER_STATUS_ACTIVE = "ACTIVE";
	public static final String USER_STATUS_INACTIVE = "INACTIVE";
	
	public static final String API_ATTRIBUTE_ACTION_TYPE_PROPERTY = "type";
	public static final String API_ATTRIBUTE_ACTION_TYPE_REPLACE = "REPLACE";
	public static final String API_ATTRIBUTE_ACTION_TYPE_ADD = "ADD";
	public static final String PAYLOAD_VALUES = "values";
	public static final String PAYLOAD_MODS = "mods";
	
	

	
}
