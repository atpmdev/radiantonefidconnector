/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.provisioning.services.RadiantoneFIDConnectionInterface;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;

/**
 * @author soori
 *
 */
public class ReconUsersTest {
	private Map<String, String> connectionParams = null;
	Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void userReconTest() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getAllUsersWithCallback(options, 0, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userReconTestToCombineFaciltyCodeandBadgeId() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getAllUsersWithCallback(options, 0, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userIncrementalReconTest() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getIncrementalUsersWithCallback(new Date(), options, 0, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userIncrementalMonthReconTest() throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getIncrementalUsersWithCallback(cal.getTime(), options, 0, null, callback);
		assertTrue(true);
	}

	@Test
	public void userReconTestInvalidEndpoint() throws Exception {
		connectionParams.put("baseURL", "https://rs2tech.com:7777/v2.0/");
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();
		connectionInterface.getAllUsersWithCallback(options, 0, null, callback);
		assertTrue(true);

	}

}
