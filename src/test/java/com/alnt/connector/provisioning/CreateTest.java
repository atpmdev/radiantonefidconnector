/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RadiantoneFIDConnectorConstants;
import com.alnt.connector.provisioning.services.RadiantoneFIDConnectionInterface;

/**
 * @author soori
 *
 */
public class CreateTest {
	private Map<String, String> connectionParams = null;
	Properties p = new Properties();

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	
	

	@Test
	public void createUserWithMandatoryData() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RadiantoneFIDConnectorConstants.USER_POSTALCODE,"postalCode");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_ST,"st");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_STREET,"street");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DEPARTMENT_NUMBER,"departmentNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DISPLAYNAME,"displayName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DIVISION_OU,"ou");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MAIL,"mail");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_EMPLOYEENUMBER,"employeeNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DESCRIPTION,"description");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MANAGER,"manager");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_FAMILYNAME_SN,"Wonderland");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_COMMON_NAME_CN,"Alice Wonderland");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_GIVENNAME,"givenName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_ORGANIZATION_O,"o");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_USERPASSWORD,"userPassword");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_PREFERREDLANGUAGE,"preferredLanguage");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_TITLE,"title");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_USERNAME_UID,"alice");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_EMPLOYEETYPE,"employeeType");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MOBILE,"mobile");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_TELEPHONE_NUMBER,"telephoneNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_HOME_PHONE,"homePhone");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_CREATED_TIMESTAMP,"createdTimeStamp");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MODIFERS_NAME,"modifersName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_CREATORS_NAME,"creatorsName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MODIFIED_TIMESTAMP,"modifiedTimeStamp");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_BASE_DN, "cn=config"); 
		userParameters.put(RadiantoneFIDConnectorConstants.OBJECTCLASS, null);
		//userParameters.put(RadiantoneFIDConnectorConstants.OBJECTCLASS, "");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
	   connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	
	/**                        SAMPLE PAYLOAD  in DOCUMENTATION
	{ 
	   "params":{ 
	      "dn":"uid=alice,cn=config",
	      "attributes":{ 
	         "objectClass":["top","person","organizationalPerson","inetOrgPerson"],
	         "cn":"Alice Wonderland",
	         "sn":"Wonderland",
	         "uid":"alice",
	         "phone":"123123"
	      }
	   }
	}
**/
	
	@Test
	public void createUserBaseDnString() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RadiantoneFIDConnectorConstants.USER_POSTALCODE,"postalCode");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_ST,"st");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_STREET,"street");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DEPARTMENT_NUMBER,"departmentNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DISPLAYNAME,"displayName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DIVISION_OU,"ou");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MAIL,"mail");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_EMPLOYEENUMBER,"employeeNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DESCRIPTION,"description");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MANAGER,"manager");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_FAMILYNAME_SN,"Wonderland");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_COMMON_NAME_CN,"Alice Wonderland");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_GIVENNAME,"givenName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_ORGANIZATION_O,"o");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_USERPASSWORD,"userPassword");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_PREFERREDLANGUAGE,"preferredLanguage");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_TITLE,"title");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_USERNAME_UID,"alice");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_EMPLOYEETYPE,"employeeType");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MOBILE,"mobile");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_TELEPHONE_NUMBER,"telephoneNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_HOME_PHONE,"homePhone");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_CREATED_TIMESTAMP,"createdTimeStamp");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MODIFERS_NAME,"modifersName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_CREATORS_NAME,"creatorsName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MODIFIED_TIMESTAMP,"modifiedTimeStamp");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_BASE_DN, "config"); 
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
	   connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void createWithBaseDnString() throws Exception {
		connectionParams.put(RadiantoneFIDConnectorConstants.BASE_DN,"accounts");
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RadiantoneFIDConnectorConstants.USER_POSTALCODE,"postalCode");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_ST,"st");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_STREET,"street");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DEPARTMENT_NUMBER,"departmentNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DISPLAYNAME,"displayName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DIVISION_OU,"ou");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MAIL,"mail");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_EMPLOYEENUMBER,"employeeNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DESCRIPTION,"description");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MANAGER,"manager");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_FAMILYNAME_SN,"Wonderland");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_COMMON_NAME_CN,"Alice Wonderland");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_GIVENNAME,"givenName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_ORGANIZATION_O,"o");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_USERPASSWORD,"userPassword");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_PREFERREDLANGUAGE,"preferredLanguage");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_TITLE,"title");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_USERNAME_UID,"alice");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_EMPLOYEETYPE,"employeeType");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MOBILE,"mobile");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_TELEPHONE_NUMBER,"telephoneNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_HOME_PHONE,"homePhone");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_CREATED_TIMESTAMP,"createdTimeStamp");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MODIFERS_NAME,"modifersName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_CREATORS_NAME,"creatorsName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MODIFIED_TIMESTAMP,"modifiedTimeStamp");
		userParameters.put(RadiantoneFIDConnectorConstants.OBJECTCLASS, "top");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
	   connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	@Test
	public void createWithBaseDnObject() throws Exception {
		connectionParams.put(RadiantoneFIDConnectorConstants.BASE_DN,"cn=accounts");
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RadiantoneFIDConnectorConstants.USER_POSTALCODE,"postalCode");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_ST,"st");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_STREET,"street");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DEPARTMENT_NUMBER,"departmentNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DISPLAYNAME,"displayName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DIVISION_OU,"ou");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MAIL,"mail");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_EMPLOYEENUMBER,"employeeNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_DESCRIPTION,"description");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MANAGER,"manager");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_FAMILYNAME_SN,"Wonderland");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_COMMON_NAME_CN,"Alice Wonderland");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_GIVENNAME,"givenName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_ORGANIZATION_O,"o");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_USERPASSWORD,"userPassword");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_PREFERREDLANGUAGE,"preferredLanguage");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_TITLE,"title");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_USERNAME_UID,"alice");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_EMPLOYEETYPE,"employeeType");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MOBILE,"mobile");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_TELEPHONE_NUMBER,"telephoneNumber");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_HOME_PHONE,"homePhone");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_CREATED_TIMESTAMP,"createdTimeStamp");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MODIFERS_NAME,"modifersName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_CREATORS_NAME,"creatorsName");
		userParameters.put(RadiantoneFIDConnectorConstants.USER_MODIFIED_TIMESTAMP,"modifiedTimeStamp");
		userParameters.put(RadiantoneFIDConnectorConstants.OBJECTCLASS, "top;person;organizationalPerson;inetOrgPerson;");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
	   connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
}
