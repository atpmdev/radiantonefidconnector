package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RadiantoneFIDConnectorConstants;
import com.alnt.connector.provisioning.services.RadiantoneFIDConnectionInterface;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;

/**
 * 
 * @author soori
 *
 */
public class ChangeAccessTest {
	private Map<String, String> connectionParams = null;
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _alertAppDateFormat = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		_baseUrl = (String) connectionParams.get(RadiantoneFIDConnectorConstants.BASE_URL);
		_apiUserName = (String) connectionParams.get(RadiantoneFIDConnectorConstants.API_USER_NAME);
		_apiPassword = (String) connectionParams.get(RadiantoneFIDConnectorConstants.API_PASSWORD);
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * if both provisioning roles & deprovisioning roles are null/ empty should it
	 * return success or failure.
	 */

	@Test
	public void changeAccessWithoutRolesToChange() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.changeAccess(null, null, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
	}

	@SuppressWarnings({ "rawtypes" })
	@Test
	public void changeAccessForWithOutUserParam() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		List provisionRoles = new ArrayList();
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		assertEquals(response.getMsgDesc(), "UserId does not passed!!");
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Test
	public void changeAccessForWithNullUserParam() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters =null;
		List provisionRoles = new ArrayList();
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
	}

	@SuppressWarnings({ "rawtypes" })
	@Test
	public void changeAccessForWithRandomUserId() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RadiantoneFIDConnectorConstants.RADIANTONEFID_USERID, UUID.randomUUID().toString());
		List provisionRoles = new ArrayList();
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		assertEquals(response.getMsgDesc(), "User does not exist in external system");
	}

	@SuppressWarnings({ "rawtypes" })
	@Test
	public void changeAccessForWithRandomUserIdbutNoWarningsFlag() throws Exception {
		connectionParams.put(RadiantoneFIDConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RadiantoneFIDConnectorConstants.RADIANTONEFID_USERID, UUID.randomUUID().toString());
		List provisionRoles = new ArrayList();
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
	}

	@Test
	public void changeAccessAddValidRole() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = null;
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	
	@Test
	public void changeAccessAddValidRoleWithActiveDate() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = null;
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessAddValidRoleWithDeActiveDate() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles =null;
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessAddValidRoleWithBothDates() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = null;
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@SuppressWarnings("null")
	@Test
	public void changeAccessDeleteValidRoleWithBothDates() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = null;
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		List<IRoleInformation> deProvisionRoles = new ArrayList<IRoleInformation>();
		Random rand = new Random();
		deProvisionRoles.add(roles.get(rand.nextInt(roles.size())));
		response = connectionInterface.changeAccess(null, null, userParameters, null, deProvisionRoles, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessAddInValidRoleWithBothDates() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = new ArrayList<IRoleInformation>();
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessAddInValidRoleIdWithBothDates() throws Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = new ArrayList<IRoleInformation>();
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	

}
