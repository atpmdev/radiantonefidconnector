/**
 * 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.RadiantoneFIDConnectorConstants;
import com.alnt.connector.exception.RadiantoneFIDConnectorException;
import com.alnt.connector.provisioning.model.UserDetails;
import com.alnt.connector.provisioning.services.RadiantoneFIDConnectionInterface;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author soori
 *
 */
public class PrepareUserDetailsFromMapTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createuserWithMandatoryData() throws RadiantoneFIDConnectorException {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(new HashMap<String, String>());
		Map<String, Object> userParameters = new HashMap<String, Object>();

		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		UserDetails holder = connectionInterface.prepareUserDetails(userParameters);
		assertEquals(connectionInterface.getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, holder), "CreateFN");
	}

	@Test
	public void createUserWithImageWithoutType() throws RadiantoneFIDConnectorException {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(new HashMap<String, String>());
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("ImageData", "asd");
		UserDetails holder = connectionInterface.prepareUserDetails(userParameters);
		assertEquals(connectionInterface.getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, holder), "CreateFN");
		
	}

	@Test
	public void createUserWithImage() throws RadiantoneFIDConnectorException {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(new HashMap<String, String>());
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("ImageType", 1);
		userParameters.put("ImageData", "asd");
		UserDetails holder = connectionInterface.prepareUserDetails(userParameters);
		assertEquals(connectionInterface.getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, holder), "CreateFN");
	}

	@Test
	public void createUserWithFullDetails() throws RadiantoneFIDConnectorException,Exception {
		RadiantoneFIDConnectionInterface connectionInterface = new RadiantoneFIDConnectionInterface(new HashMap<String, String>());
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("UserName", "userName");
		userParameters.put("Password", "password");
		userParameters.put("FirstName", "firstName");
		userParameters.put("LastName", "lastName");
		userParameters.put("Email", "Email");
		userParameters.put("PhoneNumber", "1231233123");
		userParameters.put("DateFormat", "ddMMyyyy");
		userParameters.put("TimeFormat", "HHmmss");
		List<String> plantCodes = new ArrayList<String>();
		plantCodes.add("plant1");
		plantCodes.add("plant2");
		userParameters.put("PlantCodes", plantCodes); //List<String>
		userParameters.put("DefaultPlant", "plant1");
		//	String[] rolesInSystem = {} ;
		userParameters.put("UserGroups", plantCodes.toArray());//String[]
		userParameters.put("Enabled", true);
		userParameters.put("IsLocalAccount", true);
		userParameters.put("IsAdmin", false);
		userParameters.put("IsPlantAdministrator", true);
		userParameters.put("IsReportAdministrator", true);
		userParameters.put("IsConfigurator", true);
		userParameters.put("IsAlertAdministrator", false);
		userParameters.put("IsUser", true);
		userParameters.put("IsDocumentAdministrator", true);
		UserDetails holder = connectionInterface.prepareUserDetails(userParameters);
		assertEquals(connectionInterface.getAttributeValue(RadiantoneFIDConnectorConstants.USER_USERNAME_UID, holder), "CreateFN");
		ObjectMapper mapper = new ObjectMapper();
		String gsonString = mapper.writeValueAsString(userParameters);
	}
	
	
	

}
