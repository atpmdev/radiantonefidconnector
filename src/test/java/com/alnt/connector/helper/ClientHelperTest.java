package com.alnt.connector.helper;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.provisioning.helper.RadiantoneFIDClientHelper;
import com.alnt.connector.provisioning.helper.RadiantoneFIDURLConnection;

/**
 * 
 * @author soori
 *
 */
public class ClientHelperTest {

	String restHttpsURL = "https://rs2tech.com:7777/v1.0/";
	String restHttpsInvalidURL = "https://ivalidurl/v10.0/";
	String restHttpURL = "http://rs2tech.com:7777/v1.0/";
	String userName = "rs2";
	String password = "rs2";
	String publicKey = "691F96B26D2B1C074A047ACB60A14857";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void httpsUrlTest() throws Exception {
		RadiantoneFIDURLConnection httpsConnection = RadiantoneFIDClientHelper.getConnection(restHttpsURL, "GET", userName,
				password);
		assertNotNull(httpsConnection);
	}

	@Test
	public void httpUrlTest() throws Exception {
		RadiantoneFIDURLConnection httpConnection = RadiantoneFIDClientHelper.getConnection(restHttpURL, "POST", userName,
				password);
		assertNotNull(httpConnection);
	}
	
	@Test
	public void httpInvalidUrlTest() throws Exception {
		RadiantoneFIDURLConnection httpConnection = RadiantoneFIDClientHelper.getConnection(restHttpURL, "ERROR", userName,
				password);
		assertNotNull(httpConnection);
	}
	
	
	@Test
	public void httpsInvalidUrlTest() throws Exception {
		RadiantoneFIDURLConnection httpConnection = RadiantoneFIDClientHelper.getConnection(restHttpsInvalidURL, "ERROR", userName,
				password);
		assertNotNull(httpConnection);
		
	}

}
